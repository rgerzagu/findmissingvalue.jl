# Small parsing exercise 


The exercise can be done any language. The proposed solution should not be based on external packages or dependencies. The proposed algorithms should be "personal" !

You can find on the folder different files depending on if you want to do the exercise in C language  or in another language. in the latter case, you can use the CSV file.

The CSV file (or c + h files) contains a long array. This array has been generated as follows 
- Define a range between 1 and N (i.e [1;2;3;4;.....;100])
- We have removed  two elements on this array: The second element is the square of the first (for instance 2 and 4 such that the array is [1;3;5;6;...;100]) which now has N-2 elements 
- We have shuffled this truncated array (the array is now [85;90;3;...;42]). This new array is still with N-2 elements 

=> Question: Find the missing elements of the array !
