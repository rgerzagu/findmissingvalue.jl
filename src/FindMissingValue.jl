module FindMissingValue


# ----------------------------------------------------
# --- Dependencies 
# ---------------------------------------------------- 
using Random

# ----------------------------------------------------
# --- Constant and parameters 
# ---------------------------------------------------- 
const N = 10_000

# ----------------------------------------------------
# --- Exportations 
# ---------------------------------------------------- 
export generateArray

# ----------------------------------------------------
# --- Functions 
# ---------------------------------------------------- 
"""
Generate a linearly range  array of size FindMissingValue.N between 0 and N and randomly remove two elements. Second element is the square of the first. The array is then shuffle.
If index is given as input parameter, remove this specific index 
A,val = generateArray();
- A is an Vector{Int}(N-1)
- val ∈ 1:N 

A,val = generateArray(12)
A is an array with all elements between 1 and N-2, all elements are different and 12 and 144 are missing
"""
function generateArray(index=nothing)
    # --- Create an array linearly ascendant 
    array = collect(1:N)
    # --- Remove a random entry 
    # Index that will be removed 
    if index === nothing 
        # --- First position should be < sqrt(N) 
        maxP = round(sqrt(N)) |> Int
        # --- Possibility to force index posistion
        index0   = rand(0:maxP)
        index1   = index0^2 |> Int
    else 
        @assert index ≤ round(sqrt(N)) "Key should be lower to the square root of array size"
        index0 = index
        index1 = index^2 |> Int
    end
    # --- Delete the associated value 
    deleteat!(array,index0)
    deleteat!(array,index1-1) # -1 as array is shifted right after index0
    # --- Shuffle the array 
    array = array |> shuffle
    # --- Return a tuple with array and val to be found 
    return (array,index0,index1)
end


end
