# ----------------------------------------------------
# --- 
# This file is used to write all the test files for external use 
# ---------------------------------------------------- 


"""
Write a C files (and the associated header) based on the input array, called filename.c (and filename.h)
It exports several global variables
- SIZE  : The size of the array (as #define) 
- Fs    : Sampling frequency 
- sig   : Global float array of data
"""
function writeCFile(filename,array,N;path="./")
    # ----------------------------------------------------
    # --- Writing .c file 
    # ---------------------------------------------------- 
    myFile = open(path*"$filename.c", "w")
    # Getting size 
    sL  = length(array);
    # Ensure float32 data 
    write(myFile,"#include \"$filename.h\"
        int N = $N;
        int sig[$(N-2)] = {\n")
    for a ∈ array 
        write(myFile,"$a,\n");
    end
    write(myFile,"};");
    close(myFile);
    # ----------------------------------------------------
    # --- Writing .h file 
    # ---------------------------------------------------- 
    myFile = open(path*"$filename.h", "w")
    # Getting size 
    accr = uppercase(filename);
    write(myFile,"// Header file for $filename
        #ifndef SRC_$(accr)_H_
        #define SRC_$(accr)_H_
        #define SIZE $N
        extern int sig[$(N-2)];
        #endif");
    close(myFile);
    return sL;
end



function writeCSVFile(filename,array,N;path="./")
    # ----------------------------------------------------
    # --- Writing .csv file 
    # ---------------------------------------------------- 
    myFile = open(path*"$filename.csv", "w")
    # Getting size 
    sL  = length(array);
    for a ∈ array 
        write(myFile,"$a\n");
    end
    close(myFile);
end


using FindMissingValue

array,val = generateArray(42)

writeCFile("data",array,FindMissingValue.N;path="./files/");
writeCSVFile("data",array,FindMissingValue.N;path="./files/");

