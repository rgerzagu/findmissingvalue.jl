using FindMissingValue
using Test

@testset "Array Generation" begin
    # Generate data 
    array,val0,val1 = generateArray();
    # Output should have the size of N -2 (we have removed two item)
    @test length(array) == FindMissingValue.N - 2
    # Value shoud ∈ [0;N]
    @test val0 > 0 
    @test val0 ≤ FindMissingValue.N
    @test val0^2 == val1
end


@testset "Random removal " begin
    # Generate data 
    array,val0,val1 = generateArray()
    # Check that val is not in the array 
    @test isempty(findall( array .== val0)) == true
    @test isempty(findall( array .== val1)) == true
    # Exhaustive search of all elements 
    flag = true;
    for n ∈ 1 : FindMissingValue.N 
        # We check all other numbers are here
        if !(n == val0  || n == val1)
            if isempty(findall(array .== n))
                flag = false 
            end
        end
    end
    @test flag==true
end

@testset "Deterministic removal " begin
    # Generate data 
    key = 42;
    array,val0,val1 = generateArray(key)
    @test val0 == key
    @test val1 == key^2
    # Check that val is not in the array 
    @test isempty(findall( array .== key)) == true
    @test isempty(findall( array .== key^2)) == true
    # Exhaustive search of all elements 
    flag = true;
    for n ∈ 1 : FindMissingValue.N 
        # We check all other numbers are here
        if !(n == val0  || n == val1)
            if isempty(findall(array .== n))
                flag = false 
            end
        end
    end
    @test flag==true
end
